class Str {
    constructor(txt,txt2,len) {
   //  this.lower= txt.toLowerCase();   
   //  this.upper= txt.toUpperCase();
   //  this.capitalize = txt.split("")[0].toUpperCase() + txt.slice(1);
   //  this.reverse = txt.split("").reverse().join("");
      this.txt = txt;
      this.txt2 = txt2;
      this.len = len
}

   lower(txt) {
      return txt.toLowerCase(); 
   }
   upper(){
      return txt.toUpperCase();
   }
   capitalize(txt){
         return txt.toLowerCase().split(' ').map(function (i) {
         if (i.length > 2) {
             return i.charAt(0).toUpperCase() + i.substr(1);
         } else {
             return i;
         }
     }).join(' ');
   }
   reverse(){
      return this.txt.split('').reverse().join('');
   }
   contains(txt,txt2){
      var arr = txt.split(' ');
      return arr.includes(txt2);

      }

       random(len) {
          var len = len || 16;
         var charSet =  'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
         var randomString = '';
         for (var i = 0; i < len; i++) {
             var randomPoz = Math.floor(Math.random() * charSet.length);
             randomString += charSet.substring(randomPoz,randomPoz+1);
         }
         return randomString;
       }

       slug(txt ,txt2 ){
       var c =  txt2 || '-';
         return txt.split(' ').join(c);

       }
       count(txt){
          return txt.length;
       }
       countWord(txt){
         return txt.split(' ').length;
      }
      trim(txt,txt2,N){
         txt2 = txt2 || 100;
         N = N || '...';
         if (txt.length > txt2) {
            txt = txt.substr(0, txt2) + N;
        }
        return txt;
      }
      trimWord (txt,txt2,N){
         txt2 = txt2 || 6;
         N = N || ' '
         var maxLength = txt2 * 5;
         var trimmedString = txt.substr(0, maxLength);

         //re-trim if we are in the middle of a word
      return   trimmedString = trimmedString.substr(0, Math.min(trimmedString.length, trimmedString.lastIndexOf(" "))) + N;

      }

}



const Str1 = new Str();
// console.log(Str1.lower());
// console.log(Str1.upper());
// console.log(Str1.capitalize());
// console.log(Str1.reverse());
// console.log(Str1.contains('saya ingin makan','saya'));
// console.log(Str1.random())
// console.log(Str1.slug('Arn aad cccc cscs','#'));
// console.log(Str1.count('saaaaaa asss'));
// console.log(Str1.countWord('saaaaaa asss'));
const text = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'
// console.log(Str1.trim('Less than 100 characters',20));
//console.log(Str1.trimWord('Less than 100 characters',3));

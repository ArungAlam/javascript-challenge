'use strict';

var first = ['Behind', 'every', 'great', 'man'];
var second = ['is', 'a', 'woman'];
var third = ['rolling', 'her', 'eyes'];

var total = [' '].concat(first, second, third);
var result = total.toString();

console.log(result.split(',').join(' '));
"use strict";

var csv = require("fast-csv");
var fs = require('fs');

var stream = fs.createReadStream("data.csv");

csv.fromStream(stream, { headers: ["name", "category", "price"] }).on("data", function (data) {
    console.log([data].sort());
}).on("end", function () {
    console.log("done");
});
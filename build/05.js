"use strict";

function isPrime(num) {
    for (var i = 2; i < num; i++) {
        if (num % i === 0) {
            return false;
        }
    }
    return true;
}

function primeNum(n) {
    var arr = [2];
    for (var i = 3; i < n; i += 2) {
        if (isPrime(i)) {
            arr.push(i);
        }
    }
    return arr; // use arr result on your own
}
function grub(n) {
    var arr = [1];
    for (var i = 1; i < n; i++) {
        arr.push(i);
    }
    return arr;
}
function isEven(x) {
    if (x < 0) {
        return false;
    } else return x % 2 == 0;
}
function isOdd(x) {
    return !isEven(x);
}

// console.log(grub(1000)); //for 1-1000
// console.log(isEven(-10));
// console.log(isOdd(100));
// console.log(isPrime(100));
// console.log(primeNum(100)); // prime number 100
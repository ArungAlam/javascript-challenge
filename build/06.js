'use strict';

var text = 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.\nUt enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.\nDuis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.\nExcepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.';
var sensor = ['dolor', 'elit', 'quis', 'nisi', 'fugiat', 'proident', 'laborum'];
var censor = function () {
    function convertToAsterisk(word) {
        var asteriskSentence = '';
        for (var asterisks = 0; asterisks < word.length; asterisks++) {
            asteriskSentence += '*';
        }
        return asteriskSentence;
    }

    return function (sentence, bannedWords) {
        sentence = sentence || undefined;
        bannedWords = bannedWords || undefined;

        if (sentence !== undefined && bannedWords !== undefined) {
            for (var word = 0; word < bannedWords.length; word++) {
                sentence = sentence.replace(bannedWords[word], convertToAsterisk(bannedWords[word]));
            }
        }

        return sentence;
    };
}();
console.log(censor(text, sensor));
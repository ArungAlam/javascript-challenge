'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Str = function () {
   function Str(txt, txt2, len) {
      _classCallCheck(this, Str);

      //  this.lower= txt.toLowerCase();   
      //  this.upper= txt.toUpperCase();
      //  this.capitalize = txt.split("")[0].toUpperCase() + txt.slice(1);
      //  this.reverse = txt.split("").reverse().join("");
      this.txt = txt;
      this.txt2 = txt2;
      this.len = len;
   }

   _createClass(Str, [{
      key: 'lower',
      value: function lower(txt) {
         return txt.toLowerCase();
      }
   }, {
      key: 'upper',
      value: function upper() {
         return txt.toUpperCase();
      }
   }, {
      key: 'capitalize',
      value: function capitalize(txt) {
         return txt.toLowerCase().split(' ').map(function (i) {
            if (i.length > 2) {
               return i.charAt(0).toUpperCase() + i.substr(1);
            } else {
               return i;
            }
         }).join(' ');
      }
   }, {
      key: 'reverse',
      value: function reverse() {
         return this.txt.split('').reverse().join('');
      }
   }, {
      key: 'contains',
      value: function contains(txt, txt2) {
         var arr = txt.split(' ');
         return arr.includes(txt2);
      }
   }, {
      key: 'random',
      value: function random(len) {
         var len = len || 16;
         var charSet = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
         var randomString = '';
         for (var i = 0; i < len; i++) {
            var randomPoz = Math.floor(Math.random() * charSet.length);
            randomString += charSet.substring(randomPoz, randomPoz + 1);
         }
         return randomString;
      }
   }, {
      key: 'slug',
      value: function slug(txt, txt2) {
         var c = txt2 || '-';
         return txt.split(' ').join(c);
      }
   }, {
      key: 'count',
      value: function count(txt) {
         return txt.length;
      }
   }, {
      key: 'countWord',
      value: function countWord(txt) {
         return txt.split(' ').length;
      }
   }, {
      key: 'trim',
      value: function trim(txt, txt2, N) {
         txt2 = txt2 || 100;
         N = N || '...';
         if (txt.length > txt2) {
            txt = txt.substr(0, txt2) + N;
         }
         return txt;
      }
   }, {
      key: 'trimWord',
      value: function trimWord(txt, txt2, N) {
         txt2 = txt2 || 6;
         N = N || ' ';
         var maxLength = txt2 * 5;
         var trimmedString = txt.substr(0, maxLength);

         //re-trim if we are in the middle of a word
         return trimmedString = trimmedString.substr(0, Math.min(trimmedString.length, trimmedString.lastIndexOf(" "))) + N;
      }
   }]);

   return Str;
}();

var Str1 = new Str();
// console.log(Str1.lower());
// console.log(Str1.upper());
// console.log(Str1.capitalize());
// console.log(Str1.reverse());
// console.log(Str1.contains('saya ingin makan','saya'));
// console.log(Str1.random())
// console.log(Str1.slug('Arn aad cccc cscs','#'));
// console.log(Str1.count('saaaaaa asss'));
// console.log(Str1.countWord('saaaaaa asss'));
var text = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.';
// console.log(Str1.trim('Less than 100 characters',20));
//console.log(Str1.trimWord('Less than 100 characters',3));